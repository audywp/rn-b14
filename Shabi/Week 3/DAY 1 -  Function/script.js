// function adalah suatu aksi untuk melakukan fungsi-fungsi tertentu agar mendapatkan value yang baru

// jangan lupa panggil function

// var angka1 = 10 // angka ga boleh paling depan, nanti dianggap bilangan hexadesimal

// var angka2 = 15

// var total = angka1 + angka2

// console.log(total);

// function bisa menerima parameter dari luar yang disebut parameter

// angka1 dan angka2 adalah parameter

// function penjumlahan (angka1, angka2) {
//     console.log(angka1 + angka2)
    
// }

// penjumlahan(10, 20) // function harus dijalankan untuk bisa muncul

// penjumlahan(50, 30)

// var number = "Hello"

// parseInt (number); // berubah menjadi NaN (not a number)

// var number1 = "5"
 
// parseInt (number1); // menjadi angka


// Tugas: buat suatu function dengan aturan jika parameter pertama genap, maka parameter1 * parameter2

function aritmatika (number1, number2, fungsiSukaSuka) {
    if (fungsiSukaSuka !== undefined) {
        fungsiSukaSuka()
    }

    if (number1 % 2 === 0) {
        console.log(number1 * number2)
    } else if (number1 % 2 === 1 && number1 >= number2) {
        console.log(number1 * number2 / (number1 + number2))
    }

    if (number1 === undefined) {
        console.log("Hehh belum ada parameter")
    }
}

aritmatika(17, 5, function() {
    console.log("Halo kamu anon")
})

aritmatika(20, 10)


// parameter pertama ganjil dan tidak kurang dari parameter kedua maka parameter pertama * parameter kedua / parameter1 + parameter 2

// tipe function / function delaration
    // 1. function <nama_function> () {}
    // 2. var aritmatika = function () {}

// anonymous function adalah function tanpa nama, biasanya digunakan sebagai parameter suatu function yang lain. Bentuknya : function()

function bingung (name, name) { 
    console.log(name)
}

bingung ("Shabi", 20) // kalau dua parameter namanya sama maka akan diambil yang terakhir

// var, let, const
// arrow funtion
// hoisting
// asynchronous and synchronous

// besok object dan array