// karakter = [
//     {nama: "Shabi", umur: 23},
//     {nama: "Aya", umur: 26},
//     {jamTidur: {
//         tidur: "04:00",
//         bangun: "08:00"
//         }
//     }   
// ]

// // const cerita = {
// //     jamTidur : {
// //         tidur: "04:00",
// //         bangun: "08:00"
// //     }
// // }

// for (i = 0; i < karakter.length; i++) {
//     if (karakter[i].nama === undefined || karakter[i].umur === undefined) {
//         console.log(`Mereka baru tidur jam ${karakter[2].jamTidur.tidur} dan udah bangun lagi jam ${karakter[2].jamTidur.bangun} karena sibuk ngoding.`)
//     } else {
//     console.log("Namanya " + karakter[i].nama + ", umurnya " + karakter[i].umur)
//     }
// }

"-------------------------------------------------------"

// Array adalah suatu elemen yang memiliki banyak value

var listStudents = [
    "Fajar", 
    "Soraya", 
    "Lery", 
    "Azka", 
    ["Shabi", ["Cantik", "Jelek"]]
]

// CRUD (Create, Read, Delete, Update)

// Array Read
console.log(listStudents)
console.log(`${listStudents[4][0]} si ` + listStudents[4][1][1]);

// Array Create

// push
listStudents.push("Ninuninu"); // nambahin value di belakang
console.log(listStudents)

// unshift
// listStudents.unshift("Ninuninu"); // nambahin value di depan

// console.log(listStudents)

// nama folder dan nama file nggak boleh pakai & karena nanti dianggap parameter


// untuk memastikan value tidak berulang, ketika menambahkan value bisa memakai while
while (!listStudents.includes("Ninuninu")) {
    listStudents.unshift("Ninuninu"); // nambahin value di depan    
}

console.log(listStudents)

// Array Delete 
// shift menghilangkan value di depan
listStudents.shift()
console.log(listStudents)
// pop menghilangkan value di belakang
listStudents.pop()
console.log(listStudents)
// Array Update

listStudents[0] = "Ninuninu"
console.log(listStudents)