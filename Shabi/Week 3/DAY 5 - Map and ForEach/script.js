let RNStudents = [
    {
        name: "Shabi",
        age: 23,
        hobbies: ["Talking to the cat and the hamster", "Playing Stardew Valley", "Coding"],
        gender: "Female",
        maritalStatus: true
    },
    {
        name: "Soraya",
        age: 26,
        hobbies: ["Talking to the cat", "Cooking"],
        gender: "female",
        maritalStatus: false
    },
    {
        name: "Fajar",
        age: 22,
        hobbies: ["Watching Youtube videos", "Basketball"],
        gender: "Male",
        maritalStatus: false
    },
    {
        name: "Lery",
        age: 37,
        hobbies: ["Cooking", "Washing the dishes", "Doing laundry"],
        gender: "Male",
        maritalStatus: false
    },
    {
        name: "Azka",
        age: 25,
        hobbies: ["Eating", "Coding", "Playing games"],
        gender: "Male",
        maritalStatus: false
    }
]

// Array Read
// Get student where name is - Azka

console.log(RNStudents[4].name)

// for (let i = 0; i < RNStudents.length; i++) {
//     console.log(i);
//     console.log(RNStudents[i].name)
// }

// forEach 
// console.log(RNStudents)
// RNStudents.forEach((value, index, array) => {
//     console.log("Hallo")
// })

RNStudents.forEach((v, i) => {
    console.log("Halo "+ v.name)
})

// const hasilMap = RNStudents.map((v, i) => {
//     console.log(v.name)
//     return v
// })

const map = RNStudents.map((v, i) =>{
    return { ...v, age: v.age + 1}

})

const married = RNStudents.map((v, i) => {
    if (v.maritalStatus === true) {
        return { 
            ...v, 
            maritalStatus: false, 
            age: v.age + 1
        }
    } else {
        return { 
            ...v, 
            maritalStatus: true, 
            age: v.age - 1
        }
    }
})

console.log(map)
console.log(married)

const myApp = document.querySelector('#app')
myApp.innerHTML = RNStudents.map((v, i) {
    return `<li>${v.name}</li>`
})

.join(" ")