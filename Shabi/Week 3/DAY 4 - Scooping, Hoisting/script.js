console.log("Hello!");

// var, let, dan const
var myName = "Shabi";
// jangan dipakai lagi karena var tidak mengenal error, kalau ada var baru yang namanya sama akan ketimpa. Misalnya var dipakai untuk menentukan log in, info log in lama akan ketimpa dan hilang. Bisa jadi kita ga sadar menimpa variabel yang sudah ada.

let myName1 = "Shabii"
// let myName1 = "Shabiu"

console.log(myName1);

myName1 = "Shabiu";

console.log(myName1);
// let bisa diupdate valuenya

const myName2 = "Shabiii";
// myName2 = "Shabiuu";

console.log(myName2);
// const tidak bisa diubah valuenya


// Arrow function
function aritmatika() {
    console.log("Shabi di dalam function");
};

aritmatika();

// const penjumlahan = function() {
//     console.log("Penjumlahan")
// }

// penjumlahan()

const pengurangan = () => {
    console.log("Pengurangan")
}

pengurangan()

// perbedaan function dengan return dan function tanpa return

let myRealName;
console.log(myRealName)

myRealName = "Shabira"
console.log(myRealName)

const callMyRealName = () => {
    return myRealName;
}

const callMyRealName1 = () => {
    let myRealName1 = "Shabibi"

    return callMyRealName1
}

console.log(callMyRealName())
callMyRealName()
console.log(callMyRealName)
callMyRealName1()

let penjumlahan = (a, b) => {
    return a + b;
}

console.log(penjumlahan(10, 50))

const total = penjumlahan(10 + 50)
console.log(total)

const totalJumlah = penjumlahan(10, 50)
console.log(totalJumlah)

// not defined = belum didefinisi, undefined = error

// Scooping
// Penyekatan dalam membuat suatu variabel

let studentRN = "Azka";
studentRN = "Lery"

console.log(studentRN)

let callStudentRN = () => {
    let studentRN = "Fajar"
    return studentRN
}

console.log(callStudentRN())