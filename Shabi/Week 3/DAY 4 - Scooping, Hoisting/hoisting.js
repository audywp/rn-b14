
// console.log(halo)
let halo = "Hai tampan"

// hoisting memindahkan semua variabel tanpa isinya ke atas lalu diberikan nilai default
// kalau pakai var defaultnya undefined, kalau pake let jadi error

let woman = "beautiful"

// if (woman === beautiful) {
//     false
// } else {
//     true
// }

let maritalStatus = woman === "beautiful" ? false : true

console.log(maritalStatus)

let mySingleName = "Rara"

console.log(mySingleName)

const changeMyName = (name) => {
    mySingleName = name;
}

changeMyName("Radja");


const penjumlahan = (a, b) => {
    return a + b;
}

console.log(penjumlahan(5, 10));


// 1. Jelaskan mengapa var ditinggalkan, dan apa perbedaan let dengan const?
//2. Coba eksplor mengenai arrow function kemudian berikan pendapat teman-teman di dalam jawaban
//3. Pentingnya scooping dalam javascript?
//4. Jelaskan konsep hoisting menurut pendapat teman-teman.
//Buatlah folder Assessment lalu buatlah file doc/txt/pdf. Tuliskan jawabannya file tsb lalu kirim sebagai attachment di google classroom. Paling lambat sebelum hari Sabtu jam 23.59.
