// buat object biodata yang isinya data diri minimal 7 key lalu push ke gitlab
var biodata = {
    nama: "Shabi",
    spesies: "Manusia",
    tempatLahir: "Malang",
    tanggalLahir: "4 Juli 1998",
    alamat: "Jakarta, Indonesia",
    pendidikan: "SMA",
    kesibukan: "Belajar coding di Glints Academy",
    hobi: "Ngomong sama hamster"
}

console.log(biodata);

// DIBAWAH INI NYOBA-NYOBA AJA //
function bio() {
    document.getElementById("nama").innerText = biodata.nama;
    document.getElementById("lahir").innerText = biodata.tempatLahir + ", " + biodata.tanggalLahir;
    document.getElementById("hobi").innerText = biodata.hobi;

}

function validateForm() {
    alert("Terimakasih!");
}

biodata["peliharaan"] = "Kucing";
delete biodata.spesies;
var bioUpdate = {...biodata, hobi:"Ngomong sama kucing"};
// var bioUpdate = Object.assign(biodata, {hobi: "Ngomong sama kucing"});
console.log(bioUpdate);

// var biodata = [
//     {
//         nama: "Shabi",
//         spesies: "Manusia",
//         tempatLahir: "Malang",
//         tanggalLahir: "4 Juli 1998",
//         alamat: "Jakarta, Indonesia",
//         pendidikan: "SMA",
//         kesibukan: "Belajar coding di Glints Academy",
//         hobi: "Ngomong sama hamster"
//     },

//     {
//         nama: "Ninuninu",
//         spesies: "Hamster",
//         tempatLahir: "Jakarta",
//         tanggalLahir: "8 November 2020",
//         alamat: "Jakarta, Indonesia",
//         kesibukan: "Gali sarang tiap hari",
//         hobi: "Lari dari manusia"
//     }
// ]

// function bio() {
//     document.getElementById("nama").innerText = biodata[0].nama;
//     document.getElementById("lahir").innerText = biodata[0].tempatLahir + ", " + biodata[0].tanggalLahir;
//     document.getElementById("hobi").innerText = biodata[0].hobi;

// }

// function bio() {
//     document.getElementById("biodata").innerHTML = JSON.stringify(biodata);
// };

