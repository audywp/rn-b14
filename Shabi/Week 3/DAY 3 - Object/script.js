var listKucing = ["Opik", "Tuprut", "Trinil", "Dewo", "Eko"]

listKucing.unshift("Moci");
listKucing.push("Tomoe");

console.log(listKucing);


// Spread Operator
var mengBaru = ["Poci", ...listKucing];

console.log(mengBaru);

// Slice Operator
var pindahMeng = mengBaru.slice(0, 1);

console.log(pindahMeng);

var kucingNow = mengBaru.slice(2);

console.log(kucingNow);

var ibuAnak = mengBaru.slice(3, 7);

console.log(ibuAnak);

// buat object biodata yang isinya data diri minimal 7 key lalu push ke gitlab
