console.log("Week 4 Day 2 - OOP (Object Oriented Programming)")

console.log("Task 8:")
console.log("Buat Object menggunakan class, mengenai Kendaraan. Object harus menjelaskan spesifikasi kendaraan selengkap mungkin , lalu beri beberapa method.")

class Kendaraan {
    constructor (nama, roda, medan, muatan) {
        this.nama = nama,
        this.roda = roda,
        this.medan = medan,
        this.muatan = muatan
    }
}

const Mobil = new Kendaraan("Mobil", 4, "darat", "> 2")

console.log(`${Mobil.nama} memiliki ${Mobil.roda} roda. ${Mobil.nama} adalah kendaraan ${Mobil.medan} yang dapat mengangkut ${Mobil.muatan} orang.`)