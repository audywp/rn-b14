const student = (name, age, height, weight) => {
    return {
        name: name,
        age: age,
        height: height,
        weight: weight
    }
}

const Shabi = student("Shabi", 23, 149, 42)

console.log(Shabi.age, 'umur Shabi')

// const animal = (breed, lifespan, habitat, food) => {
//     return {
//         breed: breed,
//         lifespan: lifespan,
//         habitat: habitat,
//         food: food
//     }
// }

// const hamster = animal("Hamster", "4 years max for Roborovski", "Desert", "Omnivore")
// const kucing = animal("Cat", )
// console.log("This animal could live for:", hamster.lifespan)

class Animal {
    // breed = "Hamster";
    // lifespan = "4 years max for Roborovski";
    // habitat = "Desert";
    // food = "Omnivore";
    
    constructor (breed, lifespan, habitat, food) {
        this.breed = breed;
        this.lifespan = lifespan;
        this.habitat = habitat;
        this.food = food;
    }
} // ini bukan object tapi scoop

// class depannya huruf kapital
// class punya properties dan method

// console.log(Animal)
// console.log(new Animal())

const hamster = new Animal("Hamster", "4 years max for Roborovski", "Desert", "Omnivore")

console.log(hamster)