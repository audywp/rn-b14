﻿    1. Pendapat kita tentang HTML?
        a) Apa
           HTML adalah bahasa yang digunakan untuk membuat layout website.

        b) Mengapa
           Karena bahasa HTML dijalankan di semua browser web.
           
        c) Bagaimana
           File html memiliki akhiran .html, misalnya index.html. File ini dapat dibuat dan diubah di Visual Studio Code, Notepad, atau editor html lainnya. File html dapat diklik untuk dibuka langsung di browser web.
           
    2. Pendapat kita tentang block dan inline statement?
        a) Apa
           Block dan inline statement adalah dua karakteristik behaviour elemen-elemen html.

        b) Mengapa
           Elemen-elemen dalam html memiliki behaviour masing-masing yang menentukan bagaimana mereka ditampilkan pada laman web.
           
        c) Bagaimana
           Block: elemen html yang susunannya vertikal / turun ke bawah.
               Beberapa elemen block adalah div, h1-h6.
           Inline: elemen html yang susunannya horizontal / menyamping.
               Beberapa elemen inline adalah link dan form.

    3. Buat satu file HTML dengan form yang mempunyai minimal 5 input.
       /home/shabi/Desktop/RNCLASS-B14/Week 1/DAY 2 - HTML Dasar/Assessment/Form_5_Input.html 
