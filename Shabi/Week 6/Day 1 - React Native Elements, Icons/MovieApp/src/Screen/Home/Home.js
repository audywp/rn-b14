/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react'
import { View, StyleSheet, Text, Button, TouchableOpacity, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import axios from 'axios'
import { SafeAreaView } from 'react-native-safe-area-context';

export default function Home() {
    const [MovieList, setMovieList] = useState([]);

    const getMovieList = async () => {
        const result = await axios.get(
            'https://api.themoviedb.org/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf&page=1'
        );

        setMovieList(result.data.results);
    }

    return (
        <SafeAreaView>
            <KeyboardAwareScrollView>
                {MovieList.map((value, index) => {
                    return (
                        <View>
                            <View key={index}>
                                <Image
                                    source={{
                                        uri: 'https://image.tmdb.org/t/p/original${value.poster_path}',
                                    }}
                                    resizeMode="center"
                                    style={{
                                        height: 300,
                                        width: 300,
                                        marginBottom: 5,
                                        marginTop: 20,
                                        textAlign: 'center',
                                    }}
                                />

                                <Text>{value.original_title}</Text>
                            </View>
                        </View>
                    )
                })}
            </KeyboardAwareScrollView>
            <Button onPress={getMovieList} title="Get Movie List" />
        </SafeAreaView>

    )
}
