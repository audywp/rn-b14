/* eslint-disable prettier/prettier */
import { combineReducers } from 'redux';
import Login from '../Screen/Login/redux/Reducer'
import Home from '../Screen/Home/redux/Reducer'

export const allReducers = combineReducers({
    Login,
    Home,
})