import { createStore, CreateStore } from 'redux';
import { allReducers } from './allReducers';

import const store = createStore(allReducers);