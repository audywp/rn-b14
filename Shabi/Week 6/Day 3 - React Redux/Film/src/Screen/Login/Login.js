/* eslint-disable prettier/prettier */
import React, { useState } from 'react'
import { Button, TextInput, Alert } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default function Login(props) {

    const onLogin = () => {
        if (username === "Shabi" && password === "hello") {
            props.navigation.navigate('Home')
        } else {
            Alert.alert("Wrong username or password. Have you registered?")
        };
    };

    const moveToRegister = () => {
        props.navigation.navigate('Register')
    };

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    return (
        <SafeAreaView>
            <KeyboardAwareScrollView>
                <TextInput
                    onChangeText={text => {
                        setUsername(text)
                    }}
                    placeholder="Username"
                    placeholderTextColor="#ccc"
                />

                <TextInput
                    secureTextEntry={true}
                    onChangeText={text => {
                        setPassword(text);
                    }}
                    placeholder="Password"
                    placeholderTextColor="#ccc"
                />

                <Button onPress={onLogin} title="Login" />

                <Button onPress={moveToRegister} title="Register" />
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}