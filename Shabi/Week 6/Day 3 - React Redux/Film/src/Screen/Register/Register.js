/* eslint-disable no-extra-semi */
/* eslint-disable quotes */
/* eslint-disable semi */
/* eslint-disable prettier/prettier */
import React, { useState } from 'react'
import { SafeAreaView, Button, TextInput, Alert } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default function Register(props) {
    const onRegister = () => {
        if (username === "" && email === "" && password === "") {
            Alert.alert("Please fill out all the necessary information.")
        } else if (password === confirmPassword) {
            props.navigation.navigate('Home')
        } else {
            Alert.alert("Please enter the same password in confirm password.")
        };
    };

    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    return (
        <SafeAreaView>
            <KeyboardAwareScrollView>
                <TextInput
                    onChangeText={text => {
                        setUsername(text);
                    }}
                    placeholder="Username"
                    placeholderTextColor="#ccc"
                />

                <TextInput
                    onChangeText={text => {
                        setEmail(text);
                    }}
                    placeholder="Email"
                    placeholderTextColor="#ccc"
                />

                <TextInput
                    secureTextEntry={true}
                    onChangeText={text => {
                        setPassword(text);
                    }}
                    placeholder="Password"
                    placeholderTextColor="#ccc"
                />

                <TextInput
                    secureTextEntry={true}
                    onChangeText={text => {
                        setConfirmPassword(text);
                    }}
                    placeholder="Confirm password"
                    placeholderTextColor="#ccc"
                />

                <Button onPress={onRegister} title="Register" />
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}
