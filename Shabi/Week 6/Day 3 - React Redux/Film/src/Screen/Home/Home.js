import React, { useEffect, useState } from "react";
import {
    StyleSheet,
    Text,
    ScrollView,
    View,
    Image,
    StatusBar,
    ImageBackground,
    SafeAreaView
} from "react-native";
import axios from "axios";
import { Avatar } from "react-native-elements/dist/Avatar";
import { Icon, Rating, defaultRating } from 'react-native-elements';

import { username } from "../Login/Login";

export default function Home() {
    const [dataFilm, setDataFilm] = useState([]);
    const fetchData = async () => {
        try {
            const listFilm = await axios.get("https://api.themoviedb.org/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf&page=1");
            console.log(listFilm)
            setDataFilm(listFilm.data.results)
        } catch (error) {
            console.log(error)
        }
    };

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <SafeAreaView>
            <ScrollView>
                <ImageBackground
                    source={{ uri: 'https://mcdn.wallpapersafari.com/medium/25/26/hmksxy.jpg' }}
                    resizeMode="cover"
                    style={styles.background}>
                    <View style={styles.container}>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Avatar rounded size="medium" source={{ uri: 'https://i.ytimg.com/vi/1Ne1hqOXKKI/maxresdefault.jpg', }} />
                                <View style={{ flexDirection: "column", padding: 10 }}>
                                    <Text style={{ color: "white", fontSize: 18 }}>Hello, {username}</Text>
                                    <Text style={{ color: "white", fontSize: 10, opacity: 0.5 }}>See Your Favorite Movie</Text>
                                </View>
                            </View>
                            <View>
                                <Icon
                                    reverse
                                    name='notifications-circle-outline'
                                    type='ionicon'
                                    color='black'
                                />
                            </View>


                        </View>

                        {dataFilm?.map((value, index) => (
                            <View style={{ flexDirection: "row", justifyContent: "space-between", padding: 20 }} key={index}>
                                <Image
                                    source={{ uri: `https://image.tmdb.org/t/p/original${value.poster_path}` }}
                                    style={{ width: 150, height: 150, resizeMode: "cover", borderRadius: 20 }}
                                />

                                <View
                                    style={{ justifyContent: 'center', width: '45%', resizeMode: "contain" }}
                                >
                                    <Text style={{ color: "white", fontSize: 20, flexShrink: 1 }}> {value.original_title} </Text>
                                    <Text style={{ color: "white", fontSize: 10, opacity: 0.5, flexShrink: 1 }} > ({value.release_date}) </Text>
                                    <Rating />
                                </View>
                            </View>
                        ))}
                        <StatusBar style="auto" />
                    </View>
                </ImageBackground >
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: "center",
        height: '100%',
        width: '100%'
    },
    container: {
        padding: 20,
        overflow: "visible",
    },

});