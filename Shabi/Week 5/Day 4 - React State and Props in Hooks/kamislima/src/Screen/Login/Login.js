/* eslint-disable prettier/prettier */
import React, { useState } from 'react'
import { View, Text, Button, StyleSheet, TextInput, Alert, Image, Dimensions, KeyboardAvoidingView } from 'react-native'
import { withSafeAreaInsets } from 'react-native-safe-area-context'
import { SafeAreaView } from 'react-native-safe-area-context'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default function Login(props) {
    


    const onLogin = () => {
        if(username === "Queen Shabi" && password === "hello") {
            props.navigation.navigate('Home')
        } else {
            Alert.alert("YOU SHALL NOT PASS!")
        };
    };

    const moveToRegister = () => {
        props.navigation.navigate('Register')
    };

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const [name, setName] = useState("Veiled Undead");

    return (
        <SafeAreaView>
            <KeyboardAwareScrollView style={LoginStyle.container}>
                <Image source={require("../../Assets/Images/catKnight.jpg")} style={LoginStyle.image}/>
                <Text>State your name and the password, {name}!</Text>

                <TextInput 
                onChangeText = {text => {
                    setUsername(text)
                }}
                placeholder="Username" 
                placeholderTextColor="#ccc" 
                style={LoginStyle.TextInput} />

                <TextInput 
                secureTextEntry = {true}
                onChangeText = {text => {
                    setPassword(text);
                }} 
                placeholder="Password" 
                placeholderTextColor="#ccc" 
                style={LoginStyle.TextInput} />

                <Button onPress={onLogin} title="Login" />

                <Text>You haven't reported your identity to the Queen? Away with you!</Text>

                <Button onPress={moveToRegister} title="Register" />
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}

const win = Dimensions.get('window');
const ratio = win.width/700;

const LoginStyle = StyleSheet.create({
    container: {
        paddingHorizontal: 15,
        backgroundColor: 'white',
    },

    image: {
        width: win.width,
        height: 990 * ratio,
    },

    TextInput: {
        backgroundColor: "#eee",
        marginBottom: 10,
        color: "black",
    },
})

// Name, Email, Phone number, Password, Confirm Password (harus sama dengan password)

