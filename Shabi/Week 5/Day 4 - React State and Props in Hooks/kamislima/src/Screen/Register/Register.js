/* eslint-disable prettier/prettier */
import React, {useState} from 'react'
import { View, Text, SafeAreaView, Button, StyleSheet, TextInput, Alert, Dimensions, Image } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'


export default function Register(props) {
    const [name, setName] = useState("Veiled Undead");

    const onRegister = () => {
        if (username === "" && email === "" && password === "") {
            Alert.alert("Fill all the necessary information, peasant!")
        } else if (password === confirmPassword) {
            props.navigation.navigate('Home')
        } else {
            Alert.alert("Please be consistent with your password, " + username )
        };
    };

    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    return (
        <SafeAreaView>
            <KeyboardAwareScrollView style={Style.container}>
            <Image source={require("../../Assets/Images/catQueenWhite.jpg")} style={Style.image}/>
                <Text>Ah, another {name}. Hm? You already know what to do.</Text>

                <TextInput 
                onChangeText = {text => {
                    setUsername(text);
                }}
                placeholder="Username" 
                placeholderTextColor="#ccc" 
                style={Style.TextInput} />

                <TextInput 
                onChangeText = {text => {
                    setEmail(text);
                }}
                placeholder="Email" 
                placeholderTextColor="#ccc" 
                style={Style.TextInput} />

                <TextInput 
                secureTextEntry = {true}
                onChangeText = {text => {
                    setPassword(text);
                }} 
                placeholder="Password" 
                placeholderTextColor="#ccc" 
                style={Style.TextInput} />

                <TextInput 
                secureTextEntry = {true}
                onChangeText = {text => {
                    setConfirmPassword(text);
                }}
                placeholder="Confirm password" 
                placeholderTextColor="#ccc" 
                style={Style.TextInput} />

                <Button onPress={onRegister} title="Register" />
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}

const win = Dimensions.get('window');
const ratio = win.width/1000;
const Style = StyleSheet.create({
    container: {
        paddingHorizontal: 8,
    },

    image: {
        width: win.width,
        height: 1000 * ratio,
    },

    TextInput: {
        backgroundColor: "white",
        marginBottom: 10,
        color: "black",
    },
});

// Name, Email, Phone number, Password, Confirm Password (harus sama dengan password)