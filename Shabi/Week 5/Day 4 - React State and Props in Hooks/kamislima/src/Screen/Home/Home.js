import React from 'react'
import { View, Text, Button } from 'react-native'

export default function Home(props) {
    const moveToNotifications = () => {
        props.navigation.navigate('Notifications');
    };

    const moveToProfile = () => {
        props.navigation.navigate('Profile');
    };

    const moveToSettings = () => {
        props.navigation.navigate('Settings')
    }


    return (
        <View>
            <Button onPress={moveToNotifications} title="Notifications" />
            <Button onPress={moveToProfile} title="Profile" />
            <Button onPress={moveToSettings} title="Settings" />
        </View>
    )
}
