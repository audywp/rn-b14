import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import 'react-native-gesture-handler';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './src/Screen/Home/Home';
import Notifications from './src/Screen/Notifications/Notifications';
import Profile from './src/Screen/Profile/Profile';
import Settings from './src/Screen/Settings/Settings';
import Login from './src/Screen/Login/Login';
import Register from './src/Screen/Register/Register';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>{
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Register" component ={Register} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Notifications" component={Notifications} />
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="Settings" component={Settings} />
      </Stack.Navigator>
    }</NavigationContainer>
  );
}