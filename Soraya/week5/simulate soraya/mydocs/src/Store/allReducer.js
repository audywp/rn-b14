import {combineReducers} from 'redux';
import {LoginReducer} from '../Screen/Login/Redux/Reducer';
import {RegisterReducer} from '../Screen/Register/Redux/Reducer';

export const allReducer = combineReducers({
  //function yang parameter nya objek
  LoginReducer,
  RegisterReducer,
});
