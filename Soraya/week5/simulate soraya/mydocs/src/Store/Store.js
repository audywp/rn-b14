import {createStore} from 'redux';

import {allReducer} from './allReducer';
export const Store = createStore(allReducer);
