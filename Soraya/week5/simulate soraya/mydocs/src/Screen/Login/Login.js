import React, {useState} from 'react';
import {
  Text,
  SafeAreaView,
  TextInput,
  Button,
  StyleSheet,
  Alert,
} from 'react-native';

import {TouchableOpacity} from 'react-native-gesture-handler';

import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

import {setEmailToLoginReducer} from './Redux/ActionLogin';
import {setPasswordToLoginReducer} from './Redux/ActionLogin';
import {setAccesTokenToLoginReducer} from './Redux/ActionLogin';

import {useDispatch, useSelector} from 'react-redux';

export default function Login(props) {
  const dispatch = useDispatch();
  const state = useSelector(state => state);

  const [Email, setEmail] = useState('');
  //name itu valuenya
  const [Password, setPassword] = useState('');

  const onLogin = () => {
    if (Email === 'soraya27@gmail.com' && Password === 'semangat') {
      props.navigation.navigate('HomePage');
    } else {
      Alert.alert('Error', 'Email dan Password salah');
    }
  };

  const ForgotyourPassword = () => {
    props.navigation.navigate('Register');
  };

  const sendToReducer = () => {
    dispatch(setEmailToLoginReducer(Email));
    dispatch(setPasswordToLoginReducer(Password));

    dispatch(setAccesTokenToLoginReducer('dgftkf654jio'));
  };

  console.log(state);

  return (
    <SafeAreaView>
      <TextInput
        onChangeText={text => setEmail(text)}
        placeholderTextColor="#ccc"
        placeholder="Email"
        style={LoginStyle.textInput}
      />
      <TextInput
        onChangeText={text => setPassword(text)}
        placeholderTextColor="#ccc"
        secureTextEntry={true}
        placeholder="Password"
        style={LoginStyle.textInput}
      />
      <Button
        onPress={onLogin}
        containerStyle={LoginStyle.bordir}
        title="Login"
      />
      <Button
        onPress={sendToReducer}
        containerStyle={LoginStyle.bordar}
        title="SIGN OUT"
      />

      <TouchableOpacity onPress={ForgotyourPassword}>
        <Text>Forgot your Password?</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}

const LoginStyle = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
  textInput: {
    backgroundColor: 'white',
    marginBottom: 10,
  },
  bordar: {
    borderRadius: 20,
    marginBottom: 10,
  },

  bordir: {
    borderRadius: 20,
    marginBottom: 10,
  },

  border: {
    borderRadius: 20,
    marginBottom: 10,
  },
});
