export const setPasswordToLoginReducer = payload => {
  return {
    type: 'SET_PASSWORD_TO_LOGIN_REDUCER',
    payload,
  };
};

export const setEmailToLoginReducer = payload => {
  return {
    type: 'SET_USERNAME_TO_LOGIN_REDUCER',
    payload,
  };
};

export const setAccesTokenToLoginReducer = payload => {
  return {
    type: 'SET_TOKEN',
    payload,
  };
};
