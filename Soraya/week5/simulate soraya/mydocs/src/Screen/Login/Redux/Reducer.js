const InitialState = {
  Email: '',
  Password: '',
  acces_token: '',
  loading: false,
  isLogged: false,
};

export const LoginReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_PASSWORD_TO_LOGIN_REDUCER':
      return {
        ...state,
        Password: action.payload,
      };
    default:
      return state;
  }
};
