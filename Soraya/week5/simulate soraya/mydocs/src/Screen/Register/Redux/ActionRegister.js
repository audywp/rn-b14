export const setNameToRegisterReducer = payload => {
  return {
    type: 'SET_NAME_TO_REGISTER_REDUCER',
    payload,
  };
};

export const setPhoneToRegisterReducer = payload => {
  return {
    type: 'SET_PHONE_TO_REGISTER_REDUCER',
    payload,
  };
};

export const setUsernameToRegisterReducer = payload => {
  return {
    type: 'SET_USERNAME_TO_REGISTER_REDUCER',
    payload,
  };
};

export const setEmailToRegisterReducer = payload => {
  return {
    type: 'SET_EMAIL_TO_REGISTER_REDUCER',
    payload,
  };
};

export const setPasswordToRegisterReducer = payload => {
  return {
    type: 'SET_PASSWORD_TO_REGISTER_REDUCER',
    payload,
  };
};

export const setConfirmPasswordToRegisterReducer = payload => {
  return {
    type: 'SET_CONFIRMPASSWORD_TO_REGISTER_REDUCER',
    payload,
  };
};
