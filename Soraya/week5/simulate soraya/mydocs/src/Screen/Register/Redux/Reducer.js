const InitialState = {
  name: [],
  phone: [],
  userName: [],
  email: [],
  password: [],
  confirmPassword: [],
};

export const RegisterReducer = (state = InitialState, action) => {
  switch (action.type) {
    case 'SET_NAME_TO_REGISTER_REDUCER':
      return {
        ...state,
        name: action.payload,
      };
    default:
      return state;
  }
};
