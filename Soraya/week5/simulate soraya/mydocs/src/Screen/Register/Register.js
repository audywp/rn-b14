import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Button,
  StyleSheet,
  TextInput,
  Alert,
} from 'react-native';

import {setNameToRegisterReducer} from './Redux/ActionRegister';
import {setPhoneToRegisterReducer} from './Redux/ActionRegister';
import {setUsernameToRegisterReducer} from './Redux/ActionRegister';
import {setEmailToRegisterReducer} from './Redux/ActionRegister';
import {setPasswordToRegisterReducer} from './Redux/ActionRegister';
import {setConfirmPasswordToRegisterReducer} from './Redux/ActionRegister';

import {useDispatch, useSelector} from 'react-redux';

export default function Register(props) {
  const dispatch = useDispatch();
  const state = useSelector(state => state);

  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const onRegister = () => {
    if (username === '' && email === '' && password === '') {
    } else if (password === confirmPassword) {
      props.navigation.navigate('HomePage');
    } else {
      Alert.alert('Error', 'Password masih salah');
    }
  };

  const sendToReducer = () => {
    dispatch(setNameToRegisterReducer(name));
    dispatch(setPhoneToRegisterReducer(phone));
    dispatch(setUsernameToRegisterReducer(username));
    dispatch(setEmailToRegisterReducer(email));
    dispatch(setPasswordToRegisterReducer(password));
    dispatch(setConfirmPasswordToRegisterReducer(confirmPassword));
  };

  return (
    <SafeAreaView>
      <View style={RegStyle.container}>
        <Text>Hello, Guys</Text>

        <TextInput
          onChangeText={text => {
            setName(text);
          }}
          placeholder="Name"
          placeholderTextColor="#ccc"
          style={RegStyle.TextInput}
        />

        <TextInput
          onChangeText={text => {
            setPhone(text);
          }}
          placeholder="Phone"
          placeholderTextColor="#ccc"
          style={RegStyle.TextInput}
        />

        <TextInput
          onChangeText={text => {
            setUsername(text);
          }}
          placeholder="Username"
          placeholderTextColor="#ccc"
          style={RegStyle.TextInput}
        />

        <TextInput
          onChangeText={text => {
            setEmail(text);
          }}
          placeholder="Email"
          placeholderTextColor="#ccc"
          style={RegStyle.TextInput}
        />

        <TextInput
          secureTextEntry={true}
          onChangeText={text => {
            setPassword(text);
          }}
          placeholder="Password"
          placeholderTextColor="#ccc"
          style={RegStyle.TextInput}
        />

        <TextInput
          secureTextEntry={true}
          onChangeText={text => {
            setConfirmPassword(text);
          }}
          placeholder="Confirm password"
          placeholderTextColor="#ccc"
          style={RegStyle.TextInput}
        />

        <Button onPress={onRegister} title="Register" />
        <Button onPress={sendToReducer} title="SIGN OUT" />
      </View>
    </SafeAreaView>
  );
}

const RegStyle = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
  TextInput: {
    backgroundColor: 'white',
    marginBottom: 10,
    color: 'black',
  },
});
