import 'react-native-gesture-handler';
import React from 'react';
import {View, Text} from 'react-native';
import HomePage from './src/Screen/HomePage/HomePage/';
import Login from './src/Screen/Login/Login';
import Register from './src/Screen/Register/Register';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {Provider} from 'react-redux';
import {Store} from './src/Store/Store';

const Stack = createStackNavigator(); //create stack navigator adalah function yang mereturn value maka ditampung di variabel stack

export default function App() {
  return (
    <Provider store={Store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen component={HomePage} name="HomePage" />
          <Stack.Screen component={Login} name="Login" />
          <Stack.Screen component={Register} name="Register" />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
