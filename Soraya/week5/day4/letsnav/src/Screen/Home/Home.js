/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react'; //mennambahkan variabel dengan state
import {
  View,
  Text,
  Button,
  SafeAreaView,
  ScrollView,
  Image,
} from 'react-native';
import axios from 'axios';

export default function Home(props) {
  const [listMovie, setListMovie] = useState([]);

  const getListMovie = async () => {
    const result = await axios.get(
      `https://api.themoviedb.org/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf&page=1`, //ini menggunakan function/metode
    );

    setListMovie(result.data.results);
  };

  console.log(listMovie);

  return (
    <SafeAreaView>
      <ScrollView>
        {listMovie.map((value, index) => {
          return (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'brown',
              }}
              key={index}>
              <Image
                source={{
                  uri: `https://image.tmdb.org/t/p/original${value.poster_path}`,
                }}
                resizeMode="center" //beda center dan contain
                style={{
                  height: 300,
                  width: 300,
                  marginBottom: 5,
                  marginTop: 20,
                }}
              />
              <Text>{value.original_title}</Text>
            </View>
          );
        })}
      </ScrollView>
      <Button onPress={getListMovie} title="Get List Movie" />
    </SafeAreaView>
  );
}
