import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  Button,
  StyleSheet,
  Alert,
} from 'react-native';

import {TouchableOpacity} from 'react-native-gesture-handler';

import FontAwesome from 'react-native-vector-icons/Entypo';

import {moderateScale} from 'react-native-size-matters';

export default function Login(props) {
  const [username, setUserName] = useState('');
  //name itu valuenya
  const [password, setPassword] = useState('');

  const onLogin = () => {
    if (username === 'RNB14' && password === 'semangat') {
      props.navigation.navigate('Home');
    } else {
      Alert.alert('Error', 'Username dan Password salah');
    }
  };

  const moveToRegister = () => {
    props.navigation.navigate('Register');
  };

  return (
    <SafeAreaView>
      <View style={LoginStyle.container}>
        <Text>Silakan Masuk</Text>

        <TextInput
          onChangeText={text => setUserName(text)} //onchange text menerima value dr function, text itu menampung semua isis text yang diketik di layarnya
          placeholderTextColor="#ccc"
          placeholder="Username"
          style={LoginStyle.textInput}
        />

        <TextInput
          onChangeText={text => setPassword(text)}
          placeholderTextColor="#ccc"
          secureTextEntry={true}
          placeholder="Password"
          style={LoginStyle.textInput}
        />

        <Button
          onPress={onLogin}
          containerStyle={LoginStyle.borad}
          title="Login"
        />

        <FontAwesome
          style={{alignSelf: 'center'}}
          name="edit-2"
          rightIcon={{type: 'fontawesome', name: 'bullseye'}}
          color="red"
          size={60}
        />

        <TouchableOpacity onPress={moveToRegister}>
          <Text>Move To Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const LoginStyle = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
  textInput: {
    backgroundColor: 'white',
    marginBottom: 10,
  },
  borad: {
    borderRadius: 20,
  },
});
