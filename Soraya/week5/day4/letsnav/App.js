import 'react-native-gesture-handler';
import React from 'react';
import {View, Text} from 'react-native';
import Home from './src/Screen/Home/Home';
import Login from './src/Screen/Login/Login';
import Register from './src/Screen/Register/Register';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator(); //create stack navigator adalah function yang mereturn value maka ditampung di variabel stack

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen component={Home} name="Home" />
        <Stack.Screen component={Login} name="Login" />
        <Stack.Screen component={Register} name="Register" />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
