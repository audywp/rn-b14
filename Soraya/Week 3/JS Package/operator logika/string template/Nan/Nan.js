//jika data string yang kita coba konversi bukan data yang valid, maka hasil dari konversi tersebut adalah Nan (Not a Number)
// nan adlah number spesial yang menyebutkan bahwa ini bukanlah number
//jika Nan dioperasikan dengan data number lainnya, ,maka hasilnya akan menjadi Nan lagi.

// document.writeln(`<p>${parseInt("soraya")}</p>`)//soraya bukan number
// document.writeln(`<p>${parseFloat("2.5 em")}</p>`)//dia mentolerir nilai awal tidak peduli dengan nilai belakang
// document.writeln(`<p>${Number("1.aku")}</p>`) //tidak mentolerir kesalahan satu apapun, jadi kalo nilai belakang salah maka tetap salah

//operasi pada NAn
// const value1 = Number("200");// Nan
// const value2 = 100;
// const sum = value1 + value2; //nan
// document.writeln(`<p>${sum}</p>`);

//Operasi Isnan
// untuk mengecek apkah sebuah number itu Nan atau bukan, untuk ngecek, gunakan function isnan (number). hasilnya berupa data boolen, true jika Nan, false jika bukan
const first = Number("salah"); // Nan
const totalNumber = first + 100;
document.writeln(`<p>${totalNumber}</p>`);
document.writeln(`<p>${isNan(totalNumber)}</p>`);
