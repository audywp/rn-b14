//undefined adalah keyword d JS, Undefined sebuah tipe data.
//sebuah variable yg belum ditambahkan nilai, itu merupakan tipe undefined
//undefined bebeda ddengan null di bhs pemrograman lain


//kode undefined variable
// let name;//belum dimasukkan data jadi undefined. 
// if (name === undefined) {
//     alert("UNDEFINED");

// }else{
//     alert("DEFINED");
// }


// let name = "Soraya"; //(let adalah variabel)
// if (name === undefined) {
//     alert("UNDEFINED");

// }else{
//     alert("DEFINED");
// }

// let names = "Soraya"; //(let adalah variabel)
// if (names === undefined) {
//     console.log("UNDEFINED");

// }else{
//     console.log("DEFINED");
// }


//kode undefined array value
// const names = ["eko", "soraya"];
// if (names[3] === undefined){ //mengakses data dengan index yg tdak ada
//     console.log("ARRAY UNDEFINED");

// }else{
//     console.log("ARRAY DEFINED");
// }

//kode undefinde object property
// let orang = {
//     names : "Soraya"
// };
// if (orang.names === undefined){ //kalo if pake {}
//     alert("Hello orang");

// }else{
//     alert(`Hello ${orang.names}`);
// }

//null
//null merupakan representasi data kosong, null beda dengan undefined, null berarti variabel sdh ditambahkan valuenya. hanya saja valueny null
//undefined adlh variabel belum ditambahkan value apapun
let firstName = null;//variabel yg ada isinya
if (firstName === undefined){ 
    alert("undefined"); //false
}else if (firstName === null){
    alert("null");
}else{
    alert(firstName);
}

