// const orang= {}; //objek kosong yg tidak memiliki atribut/ propereti
// //menmabha tau mengubah
// orang["nama"] = "Soraya";// di dalam kurung kotak itu namanya index
// orang["alamat"] = "jakarta";//setelah sama dengan itu namanya "value" menggunakan string
// orang["umur"] = "23";
// console.table(orang);
// delete orang["umur"];
// console.table(orang);

//membuat objek dengan properties
// const orang = {
//     "nama lengkap": "Soraya",
//     alamat:"indonesia",
//     umur : "30",

// };

//mengakses properti objek
// const orang = {
//     "nama lengkap": "Soraya", //nama lengkap (key), value (hasilnya)
//     alamat:"indonesia",
//     umur : "20",

// };

// console.log(`Nama : ${orang["nama lengkap"]}`);//kalo nama nya lebih dari stu misalnya (nama lengkap) gunakan []
// console.log (`alamat : ${orang.alamat}`);
// console.log (`umur :${orang.umur}`);