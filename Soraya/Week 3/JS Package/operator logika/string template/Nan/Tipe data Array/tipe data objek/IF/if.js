//if salah satu keyword yg digunakan untuk percabangan
//percabagan artinya kita bisa mengeksekusi kode program trtentu ketika suatu kondisi terpenuhi(true/false). jika true kita melakukan apa dan false jika kita melakuan apa.
//hampur semua bahasa pemrograman mendukung if expression

const examBiologi = 50;
if (examBiologi> 80){
    document.writeln("Good Job : A");// menampilkan ekspresi di browser
}else if (examBiologi < 80){
    document.writeln("Not Bad : B");
}else if (examBiologi<60) {
    document.writeln("Not Bad : C");
}else{
    document.writeln("Try Again Next Meeting");
}



//Else expression
//blok if akan dieksekusi ketika kondisi if bernilai true
//kadang kita ingin melakukan eksekusi program trtentu jika kondisi if bernilai false
//hal ini bisa dilakukan menggunakan else expression


