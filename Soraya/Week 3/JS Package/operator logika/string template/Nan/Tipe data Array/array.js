//array tipe data yang berisikan kumpulan data
//Array di JS memiliki sifat dinamis, artinya data bisa bertambah dengan sendirinya saat kita memasukkan data ke dalam array.

//cara kerja array setiap data di array disimpan dala posiis berurutan, dimana urutan pertama dimulai dari nomor 0, setiap kita menambah adata ke array, otomatis data akan disimpan di uruttan terakhir, urutan di array kita sebut dengan index.
let arraykosong =[];
let arrayNama = ["soraya", "dita", "inka"];
const names = []
names.push("soraya");
names.push("dita", "inka");
names.push("ali", "indra", "susi", "dilham");
console.table(names);

console.log(names[0]);
console.log(names[1]);
console.log(names[6]);
console.log(names[3]);

names [3]= "cakeep";

console.table(names);

delete names[3];
console.table(names);

names.push("masuk soraya");
console.table(names);
// di dala array tidak ada batasan harus data apa, jadi kita bisa memassukkan data appaun ke dalam array. bahkan kita juga bisa memassukkan array dalam array.
names.push(1,2,3,4,5);
names.push = (["eko", "kurniawan", "kenedi"]);
console.table (names);