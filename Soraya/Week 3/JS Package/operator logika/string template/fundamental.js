// const name = "Soraya Vandela Hosea";
// const template= `Name: ${name}`;
// console.log(template);

const name = "Soraya Vandela Hosea";
const value = 80;

const template = `Name : ${name}, Lulus : ${value<90}`;
console.log(template);


// const height ="tinggi ideal";
// const value ="160"

// const template2 =`Name : ${name}, height: ${height}, ideal : ${value<190}`;
// console.log(template2);


//multiline string
//string template juga bisa digunakan utk membuat string multiline, cukup tambahkan enter di textnya
// const multiline = `Nama saya Soraya, 
// sekarang menjadi programer,
// kalo lagi gabut ya udah main game dan tidur,
// kalo lagi marah, jangan macam-macam`;

// document.writeln("<pre>")
// document.writeln(multiline);
// document.writeln("</pre>")

//konversi string dan number
//  jika seperti di bawah ini penulisannya, ini string + string menjadi string. 
// const value1 = "5";
// const value2 = 1;
// const sum = value1 + value2;
// document.writeln(`<p>${sum}</p>`);


// const value1 = parseInt("5"); // parseint merubah string ke number
// const value2 = 1;
// const sum = value1 + value2;
// document.writeln(`<p>${sum}</p>`);

// document.writeln(`<p>${parseInt("5.1")}</p>`)// parseint akan mengkonversi ini menjadi bil. bulat dan bil. belakanmgnya tidak dieksekusi
// document.writeln(`<p>${parseFloat("1.1")}</p>`)// merubah number ke string
// document.writeln(`<p>${Number("1.1")}</p>`) // ini flexible mengikuti number nya string=>string, number=>number

// // const a = 1;
// // const b = 2;
// // const total = a+b;
// // document.writeln(`<p>${total}</p>`); 

// //kalau ingin mengubah ke string
// const a = 2;
// const b = 2;
// const total =a.toString() + b.toString();

// document.writeln(`<p>${total}</p>`); 