// //ulang array
// var listStudents = ["Soraya", "Leni", "indra"];
// listStudents.push("Lili");
// listStudents.unshift("Dita");
// console.log(listStudents);

// var listNames = ["Danang", "Rosadi", "Angga", "Linda"];
// listNames.push("Diandra");
// listNames.unshift("Mayang");
// // console.log(listNames);

// //spread operator 
// //cara kita memecah semua value di dalam objek/array
// //harus sesuai dengan bracket nya atau penutupnya
// //cara menulis nya menggunakan tiitk 3 kali di awal (...)

// var newList = [...listNames, "Reni"];//ambil semua isi di listnames ke dalam ini
//  console.log(newList, "spread operator");// ...ini adalah urutan 

// //object
// //adalah sekumpulan nilai seperti array tetapi object itu pasangan dari key dan value
// var Student = {
//     Name:'Soraya',
//     age: 30,
//     height:'160 cm',
//     callheight,function (){}

//     console.log(student.height), 
// }
// student.weight = '160 kg';
// console.log(Student.Name),
//  console.log(Student.age),
//  console.log(Student.height),
//  student.callHeight();
// }
//  //{} tanda objeck
// //object create

// console.log(Student, "before create" );
// Student.weight = "60 lbs";
// console.log(student, "after create");


var Student = {
    height:'160 cm',
    age: 20,
    Name: 'audy',
    callName: function (){
        console.log(this.Name);
    },
    umurnyaBerapa: function (){
        console.log(this.age);
    },
};


//CRUD

//object READ
// console.log(Student.Name);
// console.log(Student.age);
// console.log(Student.height);
// Student.callName();
// Student.umurnyaBerapa();

//object create
console.log(Student, 'before create');

Student.weight = '60 lbs';
Student.hobiku = 'tidur';
Student.makan = true;
var newObject = {...Student, maritalStatus: true};//belum muncul

console.log(Student, 'after create');
console.log(newObject, 'after spread operator');

//update object
Student.height = '180 cm';
var newObject = {...Student, Name: 'Soraya'};
console.log(Student.height, 'after update');
console.log(newObject, 'after spread operator');


// object delete
delete Student.Name;
delete Student.height;
console.log(Student, 'after delete');




//post test
//buatlah object biodata yang isinya kelengkapan data diri
//minimal ada 7 key 
//lalu push ke gitlab