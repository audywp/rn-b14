import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Button,
} from 'react-native';
import Gifts from './src/Assets/images/Gifts-bro.png';

const App = () => {
  return (
    <View style={Styles.fullScreen}>
      {/* Container 1 */}
      <View style={Styles.topContainer}>
        <Image resizeMode="contain" style={Styles.image} source={Gifts} />
        <Text style={Styles.bold}>Jangan Lupa Berzikir</Text>
        <Text>Hari Ini Masih Sehat</Text>
      </View>

      {/* Container 2 */}
      <View>
        <TextInput placeholder="Email" style={Styles.inputan} />
        <TextInput
          secureTextEntry={true}
          placeholder="Password"
          style={Styles.inputan}
        />
      </View>

      {/* Container 3 */}
      <View>
        {/* <Button title="Login" /> */}
        <TouchableOpacity style={Styles.myButton}>
          <Text style={Styles.whiteText}>Login</Text>
        </TouchableOpacity>
      </View>

      <View>
        <Text style={Styles.boxA}>Forgot password?</Text>
      </View>

      <View>
        <Text style={Styles.boxB}>Don't have an account? Register!</Text>
      </View>
    </View>
  );
};

const Styles = StyleSheet.create({
  fullScreen: {
    flex: 1,
    backgroundColor: 'rgb(255, 255, 255)',
    paddingHorizontal: 24,
  },
  image: {
    marginBottom: 2,
    height: 180,
    width: 180,
  },
  topContainer: {
    height: '46%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bold: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  inputan: {
    paddingHorizontal: 20,
    backgroundColor: '#F6F6F6',
    marginBottom: 16,
    borderRadius: 8,
  },
  myButton: {
    backgroundColor: 'blue',
    paddingHorizontal: 16,
    paddingVertical: 12,
    borderRadius: 12,
    alignItems: 'center',
    paddingBottom: 6,
  },
  whiteText: {
    color: 'white',
  },
  boxA: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: 'purple',
    marginBottom: 120,
  },
  boxB: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    color: 'purple',
  },
});

export default App;
