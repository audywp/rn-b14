// console.log("for each");

// array

// CRUD

const listRnStudents = [
    {name: "Shabi", age: 12, hobbies: ["talking to the hamsters", "feeding homeless cats"], gender: "female", favouriteFoods: "gado-gado"},

    {name: "Soraya", age: 17, hobbies: ["talking to the hamsters", "feeding homeless cats"], gender: "female", favouriteFoods: "gado-gado"},

    {name: "fajar", age: 27, hobbies: ["talking to the hamsters", "feeding homeless men"], gender: "male", favouriteFoods: "gado-gado"},

    {name: "Lery", age: 18, hobbies: ["talking to the hamsters", "feeding homeless men"], gender: "male", favouriteFoods: "gado-gado"},

    {name: "Azka", age: 25, hobbies: ["talking to the hamsters", "feeding homeless men"], gender: "male", favouriteFoods: "gado-gado"},

    
]

// ...ini cara mengetahui jumlah object di dalam ARRAY
// console.log(listRnStudents.length);

// ...ini cara manggil secara manual UNTUK SATU OBJEK
// console.log(listRnStudents[4].name, listRnStudents[4].hobbies);

// ...ini cara manggil secara manual UNTUK LEBIH DARI SATU OJECT
// for (let index = 0; index < 3; index++) {
//     console.log(index);
//     console.log(listRnStudents[index]);
// }

// ...forEach....cara manggil yang lebih gampang.........
// listRnStudents.forEach((value, index) => {
//     console.log(value)
// });

// ...map...saudaranya forEach...
// ...ini konsep dasarnya map
// listRnStudents.map((value, index) => {
//     console.log(value)
// });

// ...map..bekerja di sistem (cara 1)
// const hasilMap = listRnStudents.map((value, index) => {
//     return {...value, age: value.age + 1};
// }
// );
// console.log(hasilMap)


const hasilMap = listRnStudents.map((value, index) => {
//     // if (value.favouriteFoods === gado-gado) {
//         return {...value, favouriteFoods: "ketoprak"}
//     } else {
//         return {...value, favouriteFoods: "gado-gado"}
//     }
//     return {...value, favouriteFoods: "ketoprak"};

return {...value, age: value.age +1}
}
);

console.log(hasilMap);

// ....perbedaan forEach dan map
