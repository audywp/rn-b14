

// const myrealName = 'Audy wp';

//.... error
// let studentsRn = 'azka';
// let studentsRn = 'audy';

// .....tidak error ketika dimasukkan ke scoop "ARROW FUNCTION"
// let studentsRN = 'azka';


// ............SCOPING.........
// ....variable di luar
// let callStudentRn = () => {
//     let studentsRn = 'audy';
//     console.log(studentsRN);
//     return studentsRN;
// }
// callStudentRn();

// // ... lery nyobain ini
// const multiplier = 2;

// function multiply(p1, p2) {
//     return (p1 * p2) * multiplier; 
// }

// {
//     function sum( p1, p2) {
//         return (p1 + p2)

//     }
// }
// console.log(multiply(200, 300));
// console.log(sum(50, 20));


// ...variable di dalam
//...LOCAL
// let multiplier = 2;

// function div(p1, p2) {
//   const multiplierDiv = 3; //local
//   console.log('call local variable::', multiplierDiv)
//   return (p1 / p2) * multiplierDiv;
// }

// function mod() {
//   console.log('call local variable from function mod::', multiplierDiv);
// }

// console.log('result::', div(200, 100));
// console.log(mod());


//...GLOBAL 
// let multiplier = 2;

// function sum(p1, p2) { 
//   return (p1 + p2) * multiplier;
// }

// function multiply(p1, p2) {
//   return p1 * p2 * multiplier;
// }
// 
// function div(p1, p2) {
//   const multiplier = 3;
//   return (p1 / p2) * multiplier;
// }

// console.log(sum(100, 200));
// console.log(multiply(200, 300));
// console.log(div(200, 100));


//.... out of the class
// ..arrays in Objects
const characters = [
  {name: "ken the ninja", age: 20},
  {name: "master wooly", age: 40},
  {name: "ben the baby ninja", age: 14},
]

// for (let i = 0; i < characters.length; i++) {
//   console.log(characters);
// }
console.log(characters.length);