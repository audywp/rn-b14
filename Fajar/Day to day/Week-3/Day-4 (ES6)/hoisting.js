// hoisting
// memindahkan semua variabel dan function ke atas scoopnya dan diberikan nilai default

console.log

let hallo = "hai fajar";

//global 
let multiplier = 2;

function sum(p1, p2) { 
  return (p1 + p2) * multiplier;
}

function multiply(p1, p2) {
  return p1 * p2 * multiplier;
}

function div(p1, p2) {
  const multiplier = 3;
  return (p1 / p2) * multiplier;
}

console.log(
  sum(100, 200)
);
console.log(multiply(200, 300));
console.log(div(200, 100));

