console.log("day 5 Map dan forEach");

// Array

// CRUD

const listRnStudents = [
  {
    name: "fajar",
    age: 22,
    hobbies: ["basketball", "watching youtube"],
    gender: "male",
    maritalStatus: false,
  },
  {
    name: "charles",
    age: 20,
    hobbies: ["swimming", "paintng"],
    gender: "male",
    maritalStatus: false,
  },
  {
    name: "Rian",
    age: 18,
    hobbies: ["cooking", "dancing"],
    gender: "female",
    maritalStatus: false,
  },
];

// array read
// get student name zian
console.log(listRnStudents[2].name, listRnStudents[2].age);
console.log(listRnStudents.length);

for (let index = 0; index < 2; index++) {
  console.log(listRnStudents[index]);
}

for (let index = 0; index < listRnStudents.length; index++) {
  console.log(listRnStudents[index]);
}

// -----------------------------------------

// forEach

// listRnStudents.forEach(() => {
//     console.log("hallo")
// })

// listRnStudents.forEach((value, index) => {
//     console.log(value)
// })

// ----------------------------------
// map

const hasilforEach = listRnStudents.forEach((value, index) => {
  console.log(value);
  return "halo gan";
});
console.log(hasilforEach);

const hasilMap = listRnStudents.map((value, index) => {
  console.log(value);
  if (value.name === "fajar") {
    return { ...value, name: "sir fajar" };
  } else {
    return { ...value };
  }
});

console.log(hasilMap);
