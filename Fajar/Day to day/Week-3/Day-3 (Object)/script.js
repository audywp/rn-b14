// spread operator
// adalah
// cara kita mengambil semua value dalam object / array
// hars sesuai dgn bracket nya atau penutupnya
// cara menulis nya menggunakan titik 3 kali diawal (...)

console.log('Hallo RN B14')

// object
// adalah sekumpulan nilai, seperti array tetapi 
// array itu pasanga key dan value

var student = {
    Name: "fajar",
    Age: 22,
    height: "170 cm",
    howOld: function () {
        console.log(this.Age)
    }
}

// CRUD

// Read
console.log(student.Name);

// Create
console.log (student, "before  create")
student.weight = "68 kg"
console.log (student, "after create")
student.howOld();
var newObject = {...student, status: false}



// Object Update
student.height = "175 cm";

newObject = {...student, Age: 23}

console.log (newObject, "after update")
console.log(student, "after spread operator")


// delete

delete student.howOld

console.log (student, "cek yg dihapuss")