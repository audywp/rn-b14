console.log("promise");

// promise (janji)

// janji terbagi jadi 2

// janji yg waktunya pasti
// kelas jam 9
// dll

// janji yg waktu nya ngga pasti
// nongkrong
// loading nya windows
// mandi
// makan
// dll

// tahap suatu janji

// pending (menunggu)

// fulfilled (terpenuhi)

// rejected (dibatalkan)

// --------------------------------------------------------------------

// membuat janji

// resolve
// rejeceted

// prosess asynchronous
// proses yg akan dijalankan tanpa melihat urutan, siapa yg cepat dia dijalankan

// proses synchronous
// menunggu process sebelumnya agar selsai baru dijalankan proses selanjutnya

let isComing;
let setIsComingValue;

const waitIsComing = new Promise((resolve, reject) => {
  setTimeout(() => {
    isComing = setIsComingValue;

    resolve("janji Terpenuhi");
  }, 4000);
});

const setIsComing = new Promise((resolve, reject) => {
  setTimeout(() => {
    setIsComingValue = "yah bro sy datang";

    resolve("is coming sudah di set");
  }, 3000);
});

const resolveMyPromise = async () => {
  await setIsComing;
  const result = await waitIsComing;
  console.log(result, "ini async await");
  console.log(isComing);
};

resolveMyPromise();

// API adalah suatu server yg biasa diakses kapan saja

// rest API
// adalah suatu aplikasi perantara antara client ke server
