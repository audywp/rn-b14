import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Button,
  StyleSheet,
  Alert,
} from 'react-native';
import {TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Input} from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {setUsernameToLoginReducer} from './redux/action';
import {useDispatch} from 'react-redux';

export default function Login(props) {
  // const [name, setName] = useState('Fajar');

  // const changeMyName = () => {
  //   setName('Harisyah fajar susila');
  // };
  const dispatch = useDispatch();

  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const OnLogin = () => {
    if (username === 'Fajar' && password === 'Lanjutgan') {
      props.navigation.navigate('Rumah');
    } else {
      Alert.alert('Error', 'Username dan password salah bos');
    }
  };

  const moveToRegister = () => {
    props.navigation.navigate('Daftar');
  };

  const changeUsernameOnReducerf = () => {
    dispatch(setUsernameToLoginReducer);
  };

  return (
    <SafeAreaView>
      <View style={LoginStyle.container}>
        <Text>Masuk Dulu Bos</Text>

        <Input
          onChangeText={text => {
            setUserName(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="username"
          style={LoginStyle.TextInput}
        />
        <Input
          onChangeText={text => {
            setPassword(text);
          }}
          secureTextEntry={true}
          placeholderTextColor="#ccc"
          placeholder="password"
          style={LoginStyle.TextInput}
        />

        <Button title="hit action" onPress={changeUsernameOnReducer} />

        <Button onPress={OnLogin} title="Login" />
        <TouchableOpacity onPress={moveToRegister}>
          <AntDesign name="caretdown" />
          <Text>Move To Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const LoginStyle = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },
});
