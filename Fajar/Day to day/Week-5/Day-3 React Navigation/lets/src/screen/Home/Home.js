import React, {useState} from 'react';
import {
  View,
  Text,
  Button,
  SafeAreaView,
  ScrollView,
  Image,
} from 'react-native';
import axios from 'axios';

export default function Home(props) {
  const [listMovie, setListMovie] = useState([]);

  const getListMovie = async () => {
    const result = await axios.get(
      'https://api.themoviedb.org/3/discover/movie?api_key=f7b67d9afdb3c971d4419fa4cb667fbf&page=1',
    );

    setListMovie(result.data.results);
  };

  console.log(listMovie);

  return (
    <SafeAreaView>
      <ScrollView>
        {listMovie.map((value, index) => {
          return (
            <View key={index}>
              <Image
                source={{
                  uri: `https://image.tmdb.org/t/p/original${value.poster_path}`,
                }}
                resizeMode="stretch"
                style={{height: 240, width: 160, marginBottom: 10}}
              />
              <Text>{value.original_title}</Text>
            </View>
          );
        })}
      </ScrollView>

      <Button onPress={getListMovie} title="Get List Movies" />
    </SafeAreaView>
  );
}
