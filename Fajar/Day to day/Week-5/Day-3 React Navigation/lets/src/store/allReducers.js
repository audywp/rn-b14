import {combineReducers} from 'redux';
import {Loginreducer} from '../screen/Login/redux/reducer';
import HomeReducer from '../screen/Home/redux/reducer';
import Register from '../screen/Register/redux/reducer';

export const allReducers = combineReducers({
  Login: Loginreducer,
  Home: HomeReducer,
  Register: Register,
});
