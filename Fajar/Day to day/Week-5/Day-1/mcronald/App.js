import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import Keamanan from './src/Assets/Images/keamanan2.png';
const App = () => {
  return (
    <View style={styles.fullscreen}>
      {/* container 1 */}
      <View style={styles.topContainer}>
        <Image style={styles.image} source={Keamanan} />
        <Text style={styles.bold}>Welcome to Konosecure</Text>
        <Text>the ninja way to secure your data</Text>
      </View>
      {/* end container 1 */}

      {/* container 2 */}
      <View>
        <TextInput
          placeholderTextColor="#674AB3"
          placeholder="Email"
          style={styles.inputan}
        />
        <TextInput
          placeholderTextColor="#674AB3"
          secureTextEntry={true}
          placeholder="Password"
          style={styles.inputan}
        />
      </View>
      {/* end container 2 */}

      {/* container 3 */}
      <View>
        {/* <Button title="login" /> */}
        <TouchableOpacity style={styles.mybutton}>
          <Text style={styles.whiteText}>Login</Text>
        </TouchableOpacity>
        <Text style={styles.forgot}>Forgot Password?</Text>
      </View>
      {/* end container 3 */}

      {/* container 4 */}
      <View>
        <Text style={styles.register}>Don't have an account? Register</Text>
      </View>

      {/* end container 4 */}
    </View>
  );
};

const styles = StyleSheet.create({
  fullscreen: {
    flex: 1,
    backgroundColor: '#eeeeee',
    paddingHorizontal: 24,
  },
  image: {
    marginBottom: 20,
    height: 200,
    width: 200,
  },
  topContainer: {
    height: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bold: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  inputan: {
    color: 'black',
    paddingHorizontal: 16,
    backgroundColor: '#F6F6F6',
    marginBottom: 16,
    borderRadius: 12,
  },
  mybutton: {
    backgroundColor: '#6E00F0',
    paddingHorizontal: 12,
    paddingVertical: 12,
    borderRadius: 12,
    alignItems: 'center',
  },
  whiteText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  forgot: {
    paddingVertical: 12,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  register: {
    height: '50%',
    textAlign: 'center',
    marginTop: '50%',
  },
});

export default App;
