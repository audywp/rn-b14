// termasuk materi day-4

// Variabel 
// var yg di beri nama lalu = type data(int,string,object,array or etc)
// wadah yang diberi nilai, isi type data (int,string,object,array or etc)

// variabel adalah suatu wadah yg memiliki/menyimpan suatu nilai dalam bentuk type data,
// dengan menggunakan var di awal nama variabel.

// type data = string, int, object, array etc

// variabel hanya dapat di isi oleh satu type data

// string adlh type data yg diawali & diakhir petik 2 atau 1
// biasanya digunakan utk menyimpan suatu text

// contoh
var myName = "Fajar";
var myLastName = 'susila';
var hari = "jum'at"
var jumlahPengunjung = "20";
var stringKosong = "";

// string kosong sring digunakan utk login
// bagus pakai petik 2


// interger / number 
// contoh

var age = 22
var jumlahPenduduk = 250

// float , interger
// contoh

var myWeight = 68.5



// boolean
var maritalStatus = false
var graduate = true


// Functions
// variable, type data, operator, comparison

// comparison
// cara kita membandingkan 2 atau lebih suatu element
// dengan kondisi terten, dengan tujuan untuk mendapatkan nilai boolean

// kondisi pada Comparison

// khusus
// ------------------------------------

// < kurang dari

// > lebih dari

// <= 


// >=



// -------------------------------------

// = mendeklarasi variabel

// == sama dengan

// === sama dengan

// comparison
// LOGIC GATE / gerbang logika / tabel kebenaran (truth table)
// && dan //

// syarat && semua statement harus true
//  2 === 2 && 5 <= 1 -> false

// syarat // selama salah satu stetement ada yg true maka mengembalikan true

// jika semua stetement salah maka -> false
// 2 ===2 // 2<=4 // 10>2 //1>5 -> true

// flow control

// comparison

// if statement
if ( myName === "fajar" ) {
    console.log("my name is harisyah fajar")
} else {
    console.log("who are you?")
}

// switch statement


// looping
// adalah cara kita mengulangi suatu hal secara rekursif, dengan tujuam utk melihat data, pastikan utk memnentukan akhir dari looping utk menghindari ledakan pada laptop/pc

// for 
    // example
    // for (var index = 0; index <10; index++ ) {

    // }

// index++ itu sama saja dengan index = index + 1
// index-- itu sama saja dengan index = index -1


// while
    // var count = 0
    // while (count < 10) {
        // console.log(count , "is good")

        // count++
    // }



// do while
    // do {
        // console.log(count, "in do while")
        // count++
    // } while (count < 10)




//------------------------------------------------------------------------------













// structure Data
//  cara kita menyimpan dan menstruktur data agar lebih mudah diakses













// flowChart

// pseudo code

// DOM

// array

// object

// class

// React JS

// React Native



console.log ("hari ini", "nama saya", myName, myLastName, "saya", age, "tahun", "sekarang", hari)

// buat sistem penilaian sederhana, 
// jika nilai antara 100 - 80 : excelent
//  jika nilai - 84 - 78 : very good
//  jika nilai 77 -65 : good
// jika nilai 64 - 50 : enough
// otherwise is bad
