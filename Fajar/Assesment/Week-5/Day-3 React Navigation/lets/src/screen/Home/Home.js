import React from 'react';
import {View, Text, Button} from 'react-native';

export default function Home(props) {
  const moveToLogin = () => {
    props.navigation.navigate('Masuk');
  };

  const moveToRegister = () => {
    props.navigation.navigate('Daftar');
  };

  return (
    <View>
      <Button onPress={moveToLogin} title="Move to Login" />
      <Button onPress={moveToRegister} title="Move to Register" />
    </View>
  );
}
