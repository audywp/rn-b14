import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Button,
  StyleSheet,
  Alert,
} from 'react-native';
import {TextInput} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function Login(props) {
  // const [name, setName] = useState('Fajar');

  // const changeMyName = () => {
  //   setName('Harisyah fajar susila');
  // };

  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const OnLogin = () => {
    if (username === 'Fajar' && password === 'Lanjutgan') {
      props.navigation.navigate('Rumah');
    } else {
      Alert.alert('Error', 'Username dan password salah bos');
    }
  };

  const moveToRegister = () => {
    props.navigation.navigate('Daftar');
  };

  return (
    <SafeAreaView>
      <View style={LoginStyle.container}>
        <Text>{username}</Text>

        <TextInput
          onChangeText={text => {
            setUserName(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="username"
          style={LoginStyle.TextInput}
        />
        <TextInput
          onChangeText={text => {
            setPassword(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="password"
          style={LoginStyle.TextInput}
        />

        <Button onPress={OnLogin} title="Login" />
        <TouchableOpacity onPress={moveToRegister}>
          <Text>Move To Register</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const LoginStyle = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },

  TextInput: {
    color: 'black',
    backgroundColor: 'white',
    marginBottom: 8,
  },
});
