import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Button,
  StyleSheet,
  Alert,
} from 'react-native';
import {TextInput} from 'react-native';

export default function Login(props) {
  const [username, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setconfirmPassword] = useState('');

  const OnLogin = () => {
    if (password === confirmPassword) {
      props.navigation.navigate('Rumah');
    } else {
      Alert.alert('Error', 'password tidak sama bos');
    }
  };

  return (
    <SafeAreaView>
      <View style={LoginStyle.container}>
        <Text>{username}</Text>

        <TextInput
          onChangeText={text => {
            setUserName(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="Username"
          style={LoginStyle.TextInput}
        />

        <TextInput
          onChangeText={text => {
            setEmail(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="Email"
          style={LoginStyle.TextInput}
        />

        <TextInput
          keyboardType={'phone-pad'}
          onChangeText={text => {
            text.replace(/[a-z]/, '');
            setPhoneNumber(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="Phone Number"
          style={LoginStyle.TextInput}
        />

        <TextInput
          secureTextEntry={true}
          onChangeText={text => {
            setPassword(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="Password"
          style={LoginStyle.TextInput}
        />

        <TextInput
          secureTextEntry={true}
          onChangeText={text => {
            setconfirmPassword(text);
          }}
          placeholderTextColor="#ccc"
          placeholder="Confirm Password"
          style={LoginStyle.TextInput}
        />

        <Button onPress={OnLogin} title="Daftar" />
      </View>
    </SafeAreaView>
  );
}

const LoginStyle = StyleSheet.create({
  container: {
    paddingHorizontal: 8,
  },

  TextInput: {
    color: 'black',
    backgroundColor: 'white',
    marginBottom: 8,
  },
});
