import 'react-native-gesture-handler';
import React from 'react';
import {View, Text} from 'react-native';
import Home from './src/screen/Home/Home';
import Login from './src/screen/Login/Login';
import Register from './src/screen/Register/Register';

import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Masuk">
        <Stack.Screen component={Home} name="Rumah" />
        <Stack.Screen component={Login} name="Masuk" />
        <Stack.Screen component={Register} name="Daftar" />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
