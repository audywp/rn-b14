console.log("This OOP Post Test");

// // OOP


class SpekMobil {
    // properties
    constructor(brand, jenis, harga, bahanBakar, transmisi, kapasitas, jarak, waktu) {
        this.brand = brand
        this.jenis = jenis
        this.harga = harga
        this.bahanBakar = bahanBakar
        this.transmisi = transmisi
        this.kapasitas = kapasitas
        this.jarak = jarak
        this.waktu = waktu
    }

    // method
    get kecepatan () {
        return `${this.jarak}` / `${this.waktu}`
    }

}

const camry = new SpekMobil("toyota", "camry", "845 juta", "bensin", "manual", "5 orang", 10800, 60)
console.log(camry)
console.log(camry.kecepatan, "KM/JAM")