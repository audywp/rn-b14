console.log("Hy My Name Is Fajar");
console.log("This Is My Bio");

var student = {
  name: "Harisyah Fajar Susila",
  age: 22,
  height: "170 cm",
  weight: "68 kg",
  Hobby: "watching Youtube, learning new skill, Basketball",
  gender: true,
  religion: "islam",
};

// read
console.log(student);

// create
student.siblings = 6;

// update
student.weight = 55;

// delete
delete student.Hobby;

// ----------------------------------------------------------------------------------------------------------------------------------------

// After CRUD
console.log(student);
